@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    EDIT THE WHOLE WEBSITE BODY TEXT INCLUDING LINKS!
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('shares.update', $share->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">:</label>
          <input type="text" class="form-control" name="share_name" value="{{ $share->share_name }}" />
        </div>
        <div class="form-group">
          <label for="price">1 IGNORE THIS FIELD:</label>
          <input type="text" class="form-control" name="share_price" value="{{ $share->share_price }}" />
        </div>
        <div class="form-group">
          <label for="quantity">1 IGNORE THIS FIELD:</label>
          <input type="text" class="form-control" name="share_qty" value="{{ $share->share_qty }}" />
        </div>

        <div class="form-group">
          <label for="bannertitle">Banner Title:</label>
          <input type="text" class="form-control" name="bannertitle" value="{{ $share->bannertitle }}" />
        </div>

          <div class="form-group">
          <label for="bannercontent">Banner Content:</label>
          <input type="text" class="form-control" name="bannercontent" value="{{ $share->bannercontent }}" />
        </div>

          <div class="form-group">
          <label for="aboutme">About Me:</label>
          <input type="text" class="form-control" name="aboutme" value="{{ $share->aboutme }}" />
        </div>

        <div class="form-group">
          <label for="background">Background:</label>
          <input type="text" class="form-control" name="background" value="{{ $share->background }}" />
        </div>

        <div class="form-group">
          <label for="education">Education:</label>
          <input type="text" class="form-control" name="education" value="{{ $share->education }}" />
        </div>


        <div class="form-group">
          <label for="name">Name:</label>
          <input type="text" class="form-control" name="name" value="{{ $share->name }}" />
        </div>

        <div class="form-group">
          <label for="age">Age:</label>
          <input type="text" class="form-control" name="age" value="{{ $share->age }}" />
        </div>

        <div class="form-group">
          <label for="achievementshead">Achievements Head:</label>
          <input type="text" class="form-control" name="achievementshead" value="{{ $share->achievementshead }}" />
        </div>

        <div class="form-group">
          <label for="achv1title">Achievement 1 title :</label>
          <input type="text" class="form-control" name="achv1title" value="{{ $share->achv1title }}" />
        </div>

          <div class="form-group">
          <label for="achv1body">Achievement 1 body :</label>
          <input type="text" class="form-control" name="achv1body" value="{{ $share->achv1body }}" />
          </div>

          <div class="form-group">
          <label for="achv2title">Achievement 2 title :</label>
          <input type="text" class="form-control" name="achv2title" value="{{ $share->achv2title }}" />
          </div>

          <div class="form-group">
          <label for="achv2body">Achievement 2 body :</label>
          <input type="text" class="form-control" name="achv2body" value="{{ $share->achv2body }}" />
          </div>

          <div class="form-group">
          <label for="achv3title">Achievement 3 title :</label>
          <input type="text" class="form-control" name="achv3title" value="{{ $share->achv3title }}" />
          </div>

          <div class="form-group">
          <label for="achv3body">Achievement 3 body :</label>
          <input type="text" class="form-control" name="achv3body" value="{{ $share->achv3body }}" />
          </div>

          <div class="form-group">
          <label for="achv4title">Achievement 4 title :</label>
          <input type="text" class="form-control" name="achv4title" value="{{ $share->achv4title }}" />
          </div>

          <div class="form-group">
          <label for="achv4body">Achievement 4 body :</label>
          <input type="text" class="form-control" name="achv4body" value="{{ $share->achv4body }}" />
          </div>

          <div class="form-group">
          <label for="achv5title">Achievement 5 title :</label>
          <input type="text" class="form-control" name="achv4title" value="{{ $share->achv5title }}" />
          </div>

          <div class="form-group">
          <label for="achv5body">Achievement 5 body :</label>
          <input type="text" class="form-control" name="achv5body" value="{{ $share->achv5body }}" />
          </div>

          <div class="form-group">
          <label for="achv6title">Achievement 6 title :</label>
          <input type="text" class="form-control" name="achv6title" value="{{ $share->achv6title }}" />
          </div>

          <div class="form-group">
          <label for="achv6title">Achievement 6 title :</label>
          <input type="text" class="form-control" name="achv6title" value="{{ $share->achv6title }}" />
          </div>

          <div class="form-group">
          <label for="achv7title">Achievement 7 title :</label>
          <input type="text" class="form-control" name="achv4title" value="{{ $share->achv7title }}" />
          </div>

          <div class="form-group">
          <label for="achv7body">Achievement 7 body :</label>
          <input type="text" class="form-control" name="achv4body" value="{{ $share->achv7body }}" />
          </div>

          <div class="form-group">
          <label for="achv8body">Achievement 8 body :</label>
          <input type="text" class="form-control" name="achv7body" value="{{ $share->achv8body }}" />
          </div>

          <div class="form-group">
          <label for="achv8body">Achievement 8 body :</label>
          <input type="text" class="form-control" name="achv4body" value="{{ $share->achv8body }}" />
          </div>

          <div class="form-group">
          <label for="achv9title">Achievement 9 title :</label>
          <input type="text" class="form-control" name="achv9title" value="{{ $share->achv9title }}" />
          </div>

          <div class="form-group">
          <label for="achv9body">Achievement 9 body :</label>
          <input type="text" class="form-control" name="achv9body" value="{{ $share->achv9body }}" />
          </div>

          <div class="form-group">
          <label for="achv10title">Achievement 10 title :</label>
          <input type="text" class="form-control" name="achv4title" value="{{ $share->achv4title }}" />
          </div>

          <div class="form-group">
          <label for="achv10body">Achievement 10 body :</label>
          <input type="text" class="form-control" name="achv10body" value="{{ $share->achv10body }}" />
          </div>

          <div class="form-group">
          <label for="achv11title">Achievement 11 title :</label>
          <input type="text" class="form-control" name="achv11title" value="{{ $share->achv11title }}" />
          </div>

          <div class="form-group">
          <label for="achv11body">Achievement 11 body :</label>
          <input type="text" class="form-control" name="achv11body" value="{{ $share->achv11body }}" />
          </div>

          <div class="form-group">
          <label for="achv12title">Achievement 12 title :</label>
          <input type="text" class="form-control" name="achv12title" value="{{ $share->achv12title }}" />
          </div>

          <div class="form-group">
          <label for="achv12body">Achievement 12 body :</label>
          <input type="text" class="form-control" name="achv12body" value="{{ $share->achv12body }}" />
          </div>

          <div class="form-group">
          <label for="phone">Phone number :</label>
          <input type="text" class="form-control" name="phone" value="{{ $share->phone }}" />
          </div>

          <div class="form-group">
          <label for="email">Email :</label>
          <input type="text" class="form-control" name="email" value="{{ $share->email }}" />
          </div>

          <div class="form-group">
          <label for="address">Address :</label>
          <input type="text" class="form-control" name="address" value="{{ $share->address }}" />
          </div>

          <div class="form-group">
          <label for="video1">Video 1 Link :</label>
          <input type="text" class="form-control" name="video1" value="{{ $share->video1 }}" />
          </div>

          <div class="form-group">
          <label for="video2">Video 2 Link :</label>
          <input type="text" class="form-control" name="video2" value="{{ $share->video2 }}" />
          </div>

          <div class="form-group">
          <label for="video3">Video 3 Link :</label>
          <input type="text" class="form-control" name="video3" value="{{ $share->video3 }}" />
          </div>

           <div class="form-group">
          <label for="video4">Video 4 Link :</label>
          <input type="text" class="form-control" name="video4" value="{{ $share->video4 }}" />
          </div>

           <div class="form-group">
          <label for="video5">Video 5 Link :</label>
          <input type="text" class="form-control" name="video5" value="{{ $share->video5 }}" />
          </div>

           <div class="form-group">
          <label for="video6">Video 6 Link :</label>
          <input type="text" class="form-control" name="video6" value="{{ $share->video6 }}" />
          </div>


           <div class="form-group">
          <label for="teamdescription">Team description</label>
          <input type="text" class="form-control" name="teamdescrption" value="{{ $share->teamdescription }}" />
          </div>

          <div class="form-group">
          <label for="team1">Team member 1</label>
          <input type="text" class="form-control" name="team1" value="{{ $share->team1 }}" />
          </div>

          <div class="form-group">
          <label for="team1t">Team member title</label>
          <input type="text" class="form-control" name="team1t" value="{{ $share->team1t }}" />
          </div>

           <div class="form-group">
          <label for="team2">Team member 2</label>
          <input type="text" class="form-control" name="team2" value="{{ $share->team2 }}" />
          </div>

          <div class="form-group">
          <label for="team2t">Team member 2 title</label>
          <input type="text" class="form-control" name="team2t" value="{{ $share->team2t }}" />
          </div>

           <div class="form-group">
          <label for="team3">Team member 3</label>
          <input type="text" class="form-control" name="team3" value="{{ $share->team3 }}" />
          </div>

          <div class="form-group">
          <label for="team3t">Team member 3 title</label>
          <input type="text" class="form-control" name="team3t" value="{{ $share->team3 }}" />
          </div>

          <div class="form-group">
          <label for="team4">Team member 4</label>
          <input type="text" class="form-control" name="team4" value="{{ $share->team4 }}" />
          </div>


          <div class="form-group">
          <label for="team4t">Team member 4 title</label>
          <input type="text" class="form-control" name="team4t" value="{{ $share->team4t }}" />
          </div>

           <div class="form-group">
          <label for="testimonyname">Testimony Owner</label>
          <input type="text" class="form-control" name="testimonyname" value="{{ $share->testimonyname }}" />
          </div>
            
             <div class="form-group">
          <label for="testimonyoc">Testimony Owner Occupation</label>
          <input type="text" class="form-control" name="testimonyoc" value="{{ $share->testimonyoc }}" />
          </div>

            <div class="form-group">
          <label for="testimony">Testimony</label>
          <input type="text" class="form-control" name="testimony" value="{{ $share->testimony }}" />
          </div>

           <div class="form-group">
          <label for="fb">Facebook link</label>
          <input type="text" class="form-control" name="fb" value="{{ $share->fb }}" />
          </div>

           <div class="form-group">
          <label for="twitter">Twitter link</label>
          <input type="text" class="form-control" name="twitter" value="{{ $share->twitter }}" />
          </div>

           <div class="form-group">
          <label for="instagram">Instagram link</label>
          <input type="text" class="form-control" name="instagram" value="{{ $share->instagram }}" />
          </div>



















        <button type="submit" class="btn btn-primary">Update</button>
      </form>

  </div>
</div>
 <td>
               <a href="{{ url('/')}}" class="btn btn-primary">GO BACK TO WEBSITE</a > 
            </td>
@endsection