<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <title>Emmanuel Bako</title>
    <link rel="icon" href="/img/team/favicon.ico" style="color:brown;"> 

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/line-icons.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/nivo-lightbox.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css">    
    <link rel="stylesheet" href="css/responsive.css">
<link rel="shortcut icon" href="http://example.com/favicon.ico" />
  </head>
  <body>

    <!-- Header Section Start -->
    <header id="hero-area" data-stellar-background-ratio="0.5">    
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a href="/" class="navbar-brand"><h5><font color="skyblue" weight="800" style="Comic Sans MS" size=5vw>EMMANUEL BAKO</font></h5></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <i class="lnr lnr-menu"></i>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#hero-area">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#services">Me</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#features">Awards</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#portfolios">Gallery</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#team">My Team</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="https://emmanuelbako299384965.wordpress.com">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#contact">Contact</a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
           <li>
              <a class="page-scroll" href="#hero-area">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="#services">Services</a>
            </li>
            <li>
              <a class="page-scroll" href="#features">Features</a>
            </li>
            <li>
              <a class="page-scroll" href="mypics">Gallery</a>
            </li>
            <li>
              <a class="page-scroll" href="#pricing">Pricing</a>
            </li>
            <li>
              <a class="page-scroll" href="#team">Team</a>
            </li>
            <li >
              <a class="page-scroll" href="#blog">Blog</a>
            </li>
            <li>
              <a class="page-scroll" href="#contact">Contact</a>
            </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->   
      <div class="container">      
        <div class="row justify-content-md-center">
          <div class="col-md-10">
            <div class="contents text-center">
              <h1 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0.3s">{{ $shares->bannertitle }}</h1>
              <p class="lead  wow fadeIn" data-wow-duration="1000ms" data-wow-delay="400ms">{{ $shares->bannercontent }}</p>
              <a href="#" class="btn btn-common wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">"Riding is a vision"</a>
            </div>
          </div>
        </div> 
      </div>           
    </header>
    <!-- Header Section End --> 

    <!-- Services Section Start -->
    <section id="services" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">About Me</h2>
          <hr class="lines wow zoomIn" data-wow-delay="0.3s">
          <p class="section-subtitle wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">{{ $shares->aboutme }}</p>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.2s">
              <div class="icon">
                <i class="lnr lnr-user"></i>
              </div>
              <div class="text">
              <h4>Profile</h4>
              <h5><font size=3vw>Full Name : {{ $shares->name }}</font></h5>
              <h5><font size=3vw>Age :<?php echo (date("Y")-2005); ?></font></h5>
              <h5><font size=3vw>Years active: 2013-present</font></h5>
            </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.8s">
              <div class="icon">
                <i class="lnr lnr-layers"></i>
              </div>
              <h4>Background</h4>
              <p>{{ $shares->background }}</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="lnr lnr-pencil"></i>
              </div>
              <h4>Education</h4>
              <p>{{ $shares->education }}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- Features Section Start -->
    <section id="features" class="section" data-stellar-background-ratio="0.2">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Achievements</h2>
          <hr class="lines">
          <p class="section-subtitle"><font >{{ $shares->achievementshead }}</font></p>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="container">
              <div class="row">
                 <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                      <h4>{{ $shares->achv1title }}</h4>
                      <p>{{ $shares->achv1body }}</p>
                    </div>
                  </div>

                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                      <h4>{{ $shares->achv2title }}</h4>
                      <p>{{ $shares->achv2body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv3title }}</h4>
                      <p>{{ $shares->achv3body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv4title }}</h4>
                      <p>{{ $shares->achv4body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv5title }}</h4>
                      <p>{{ $shares->achv5body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv6title }}</h4>
                      <p>{{ $shares->achv6body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv7title }}</h4>
                      <p>{{ $shares->achv7body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv8title }}</h4>
                      <p>{{ $shares->achv8body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv9title }}</h4>
                      <p>{{ $shares->achv9body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv10title }}</h4>
                      <p>{{ $shares->achv10body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv11title }}</h4>
                      <p>{{ $shares->achv11body }}</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <span class="icon">
                      <i class="lnr lnr-flag"></i>
                    </span>
                    <div class="text">
                     <h4>{{ $shares->achv12title }}</h4>
                      <p>{{ $shares->achv12body }}</p>
                    </div>
                  </div>
                  
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-xs-12">
            <div class="show-box">
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Features Section End -->    

    <!-- Portfolio Section -->
    <section id="portfolios" class="section">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">My Gallery</h2>
          <hr class="lines">
   
        </div>
        <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <a class="filter active btn btn-common" data-filter="all">
                All 
              </a>
              <a class="filter btn btn-common" data-filter=".racing">
                Racing
              </a>
              <a class="filter btn btn-common" data-filter=".awards">
                Awards
              </a>
              <a class="filter btn btn-common" data-filter=".family">
                Family
              </a>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>

          <!-- Portfolio Recent Projects -->
          <div id="portfolio" class="row">
             @foreach($images as $image)
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix racing">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="{{ asset('racing/' . $image->getFilename()) }}"> 
                  <a class="overlay lightbox" href="{{ asset('awards/' . $image->getFilename()) }}">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
              @endforeach

               @foreach($images1 as $image1)
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix awards">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="{{ asset('awards/' . $image1->getFilename()) }}"> 
                  <a class="overlay lightbox" href="{{ asset('awards/' . $image1->getFilename()) }}">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            @endforeach
            @foreach($images2 as $image2)
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix family">
              <div class="portfolio-item">
                <div class="shot-item">
                   <img src="{{ asset('family/' . $image2->getFilename()) }}">
                  <a class="overlay lightbox" href="{{ asset('awards/' . $image2->getFilename()) }}">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
      <!-- Container Ends -->
    </section>
    <!-- Portfolio Section Ends --> 

                  
    <!-- Start Video promo Section -->
    <section class="video-promo section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
              <div class="video-promo-content text-center">
                <h2 class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Watch me race below</h2>
                <p class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms"></p>
                <a href="{{ $shares->video1 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>&nbsp
                <a href="{{ $shares->video2 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>&nbsp
                <a href="{{ $shares->video3 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>
                 <a href="{{ $shares->video4 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>
                  <a href="{{ $shares->video5 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>
                   <a href="{{ $shares->video6 }}" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Video Promo Section -->

   
    <!-- Team section Start -->
    <section id="team" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">My Team</h2>
          <hr class="lines">
          <p class="section-subtitle">{{ $shares->description }}</p>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team1.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">{{ $shares->team1 }}</h4>
                  <p>{{ $shares->team1t }}</p>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team2.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">{{ $shares->team2 }}</h4>
                  <p>{{ $shares->team2t }}</p>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team3.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">                  
                  <h4 class="team-title">{{ $shares->team3 }}</h4>
                  <p>{{ $shares->team3t }}</p>
                 
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img class="img-fulid" src="img/team/team3.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">{{ $shares->team4 }}</h4>
                  <p>{{ $shares->team4t }}</p>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Team section End -->

    <!-- testimonial Section Start -->
    <div id="testimonial" class="section" data-stellar-background-ratio="0.1">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-12">
            <div class="touch-slider owl-carousel owl-theme">
              
              <div class="testimonial-item">
                <img src="img/testimonial/team4.jpg" alt="Client Testimonoal" />
                <div class="testimonial-text">
                  <p>{{ $shares->testimony }}</p>
                  <h3>{{ $shares->testimonyname }}</h3>
                  <span>{{ $shares->testimonyoc }}</span>
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
    </div>
    <!-- testimonial Section Start -->

   

    <!-- Contact Section Start -->
    <section id="contact" class="section" data-stellar-background-ratio="-0.2">      
      <div class="contact-form">
        <div class="container">
          <div class="row">     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-us">
                <h3>Contact Me</h3>
                <div class="contact-address">
                  <p>Harare,Zimbabwe</p>
                  <p class="phone">Phone: <span>({{ $shares->phone }})</span></p>
                  <p class="email">E-mail: <span>(me@emmanuelbako.com)</span></p>
                </div>
                <div class="social-icons">
                  <ul>
                    <li class="facebook"><a href="{{ $shares->fb }}"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="{{ $shares->twitter }}"><i class="fa fa-twitter"></i></a></li>
                    
                  </ul>
                </div>
              </div>
            </div>     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-block">
                <form id="contactForm">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your Email" id="email" class="form-control" name="name" required data-error="Please enter your email">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
                        <textarea class="form-control" id="message" placeholder="Your Message" rows="8" data-error="Write your message" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="submit-button text-center">
                        <button class="btn btn-common" id="submit" type="submit">Send Message</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>           
    </section>
    <!-- Contact Section End -->

    <!-- Footer Section Start -->
    <footer>          
      <div class="container">
        <div class="row">
          <!-- Footer Links -->
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <ul class="footer-links">
              <li>
                <a href="#hero-area">Homepage</a>
              </li>
             
              <li>
                <a href="#services">About Me</a>
              </li>
              
            </ul>
          </div>
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="copyright">
              <p>All copyrights reserved &copy; 2018 -Designed by <a rel="nofollow" href="https://twitter.com/paulkumz">Paulkumz</a></p>
            </div>
          </div>  
        </div>
      </div>
    </footer>
    <!-- Footer Section End --> 

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lnr lnr-arrow-up"></i>
    </a>
    
    <div id="loader">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>     

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="js/jquery-min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mixitup.js"></script>
    <script src="js/nivo-lightbox.js"></script>
    <script src="js/owl.carousel.js"></script>    
    <script src="js/jquery.stellar.min.js"></script>    
    <script src="js/jquery.nav.js"></script>    
    <script src="js/scrolling-nav.js"></script>    
    <script src="js/jquery.easing.min.js"></script>    
    <script src="js/smoothscroll.js"></script>    
    <script src="js/jquery.slicknav.js"></script>     
    <script src="js/wow.js"></script>   
    <script src="js/jquery.vide.js"></script>
    <script src="js/jquery.counterup.min.js"></script>    
    <script src="js/jquery.magnific-popup.min.js"></script>    
    <script src="js/waypoints.min.js"></script>    
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>   
    <script src="js/main.js"></script>

  </body>
</html>