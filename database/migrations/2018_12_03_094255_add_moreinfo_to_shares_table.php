<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreinfoToSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shares', function (Blueprint $table) {
            $table->text('team1')->nullable();
            $table->text('team2')->nullable();
            $table->text('team3')->nullable();
            $table->text('team4')->nullable();
            $table->text('team1t')->nullable();
            $table->text('team2t')->nullable();
            $table->text('team3t')->nullable();
            $table->text('team4t')->nullable();
            $table->text('testimony')->nullable();
            $table->text('testimonyoc')->nullable();
            $table->text('testimonyname')->nullable();
            $table->text('fb')->nullable();
            $table->text('twitter')->nullable();
            $table->text('instagram')->nullable();
            $table->text('teamdescription')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shares', function (Blueprint $table) {
            //
        });
    }
}
