<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorecontentToSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shares', function (Blueprint $table) {
            $table->text('achv5title')->nullable();
            $table->text('achv5body')->nullable();
             $table->text('achv6title')->nullable();
            $table->text('achv6body')->nullable();
             $table->text('achv7title')->nullable();
            $table->text('achv7body')->nullable();
             $table->text('achv8title')->nullable();
            $table->text('achv8body')->nullable();
             $table->text('achv9title')->nullable();
            $table->text('achv9body')->nullable();
             $table->text('achv10title')->nullable();
            $table->text('achv10body')->nullable();
             $table->text('achv11title')->nullable();
            $table->text('achv11body')->nullable();
             $table->text('achv12title')->nullable();
            $table->text('achv12body')->nullable();
             $table->text('achv13title')->nullable();
            $table->text('achv13body')->nullable();
            $table->text('video4')->nullable();
            $table->text('video5')->nullable();
            $table->text('video6')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shares', function (Blueprint $table) {
            //
        });
    }
}
