<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->text('bannertitle')->nullable();
            $table->text('bannercontent')->nullable();
            $table->text('aboutme')->nullable();
            $table->text('background')->nullable();
            $table->text('education')->nullable();
            $table->text('name')->nullable();
            $table->text('age')->nullable();
            $table->text('achievementshead')->nullable();
            $table->text('achv1title')->nullable();
            $table->text('achv1body')->nullable();
            $table->text('achv2title')->nullable();
            $table->text('achv2body')->nullable();
            $table->text('achv3title')->nullable();
            $table->text('achv3body')->nullable();
            $table->text('achv4title')->nullable();
            $table->text('achv4body')->nullable();
            $table->text('phone')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();
            $table->text('video1')->nullable();
            $table->text('video2')->nullable();
            $table->text('video3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
