<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'ShareController@welcome')->name('welcome');


Route::get('/home', 'HomeController@index')->name('home');

Route::post('/upload', 'UploadController@upload');



Route::resource('shares', 'ShareController');

Route::get('/image', function () {
    return view('image');
});
