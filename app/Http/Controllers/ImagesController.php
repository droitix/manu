<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;

class ImagesController extends Controller
{
   
    public function welcome()
    {

        $images = \File::allFiles(public_path('images'));

         return view('welcome')->with('images',$images);
    }
}
