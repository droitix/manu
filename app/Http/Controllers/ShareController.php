<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Share;


class ShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $shares = Share::all();

        return view('shares.index', compact('shares'));
    }

    public function welcome()
    {
       $shares = Share::first();
       
       $images = \File::allFiles(public_path('racing'));
       $images1 = \File::allFiles(public_path('awards'));
       $images2 = \File::allFiles(public_path('family'));



        return view('welcome', compact('shares', 'images','images1','images2'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'share_name'=>'required',
        'share_price'=> 'required|integer',
        'share_qty' => 'required|integer'
      ]);
      $share = new Share([
        'share_name' => $request->get('share_name'),
        'share_price'=> $request->get('share_price'),
        'share_qty'=> $request->get('share_qty')
      ]);
      $share->save();
      return redirect('/shares')->with('success', 'Website (www.emmanuelbako.com) updated now!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $share = Share::find($id);

        return view('shares.edit', compact('share'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'share_name'=>'required',
        'share_price'=> 'required|integer',
        'share_qty' => 'required|integer'
      ]);

      $share = Share::find($id);
      $share->share_name = $request->get('share_name');
      $share->share_price = $request->get('share_price');
      $share->share_qty = $request->get('share_qty');
      $share->bannertitle = $request->get('bannertitle');
      $share->bannercontent = $request->get('bannercontent');
      $share->aboutme = $request->get('aboutme');
      $share->background = $request->get('background');
      $share->education = $request->get('education');
      $share->name = $request->get('name');
      $share->age = $request->get('age');
      $share->achievementshead = $request->get('achievementshead');
      $share->achv1title = $request->get('achv1title');
      $share->achv1body = $request->get('achv1body');
      $share->achv2title = $request->get('achv2title');
      $share->achv2body = $request->get('achv2body');
      $share->achv3body = $request->get('achv3body');
      $share->achv3title = $request->get('achv3title');
      $share->achv4title = $request->get('achv4title');
      $share->achv4body = $request->get('achv4body');
      $share->achv5title = $request->get('achv5title');
      $share->achv5body = $request->get('achv5body');
      $share->achv6title = $request->get('achv6title');
      $share->achv6body = $request->get('achv6body');
      $share->achv7title = $request->get('achv7title');
      $share->achv7body = $request->get('achv7body');
      $share->achv8title = $request->get('achv8title');
      $share->achv8body = $request->get('achv8body');
      $share->achv9title = $request->get('achv9title');
      $share->achv9body = $request->get('achv9body');
      $share->achv10title = $request->get('achv10title');
      $share->achv10body = $request->get('achv10body');
      $share->achv11title = $request->get('achv11title');
      $share->achv11body = $request->get('achv11body');
      $share->achv12title = $request->get('achv12title');
      $share->achv12body = $request->get('achv12body');
      $share->achv13title = $request->get('achv13title');
      $share->achv13body = $request->get('achv13body');
      $share->phone = $request->get('phone');
      $share->email = $request->get('email');
      $share->address = $request->get('address');
      $share->video1 = $request->get('video1');
      $share->video2 = $request->get('video2');
      $share->video3 = $request->get('video3');
      $share->video4 = $request->get('video4');
      $share->video5 = $request->get('video5');
      $share->video6 = $request->get('video6');
      $share->teamdescription = $request->get('teamdescription');
      $share->team1 = $request->get('team1');
      $share->team2 = $request->get('team2');
      $share->team3 = $request->get('team3');
      $share->team4 = $request->get('team4'); 
      $share->team1t = $request->get('team1t');
      $share->team2t = $request->get('team2t');
      $share->team3t = $request->get('team3t');
      $share->team4t = $request->get('team4t'); 
      $share->testimony = $request->get('testimony');
      $share->testimonyoc = $request->get('testimonyoc'); 
      $share->testimonyname = $request->get('testimonyname');
      $share->fb = $request->get('fb');
      $share->twitter = $request->get('twitter');
      $share->instagram = $request->get('instagram');     
      $share->save();

      return redirect('/shares')->with('success', 'www.emmanuelbako.com has been updated');
}
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $share = Share::find($id);
     $share->delete();

     return redirect('/shares')->with('success', 'Stock has been deleted Successfully');
    }
}
